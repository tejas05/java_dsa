import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.geometry.Insets;

import java.io.*;
import java.net.*;


/**
 * Opens a hsg_window that can be used for a two-way network chat.
 * The hsg_window can "listen" for a connection request on a port
 * that is specified by the user.  It can request a connection
 * to another GUIChat hsg_window on a specified computer and port.
 * The hsg_window has an input box where the user can enter
 * messages to be sent over the connection.  A connection
 * can be closed by clicking a button in the hsg_window or by
 * closing the hsg_window.  To test the program, several
 * copies of the program can be run on the same computer.
 */
public class HSG_Chat_App extends Application {
    
    public static void main(String[] hsg_args) {
        launch(hsg_args);
    }
    //--------------------------------------------------------------

    /**
     * Possible states of the thread that handles the network connection.
     */
    private enum ConnectionState { LISTENING, CONNECTING, CONNECTED, CLOSED }

    /**
     * Default port number.  This is the initial content of input boxes in
     * the hsg_window that specify the port number for the connection. 
     */
    private static String hsg_defaultPort = "1501";

    /**
     * Default host name.  This is the initial content of the input box that
     * specifies the name of the computer to which a connection request
     * will be sent.
     */
    private static String hsg_defaultHost = "localhost";


    /**
     * The thread that handles the connection; defined by a nested class.
     */
    private volatile ConnectionHandler connection;

    /**
     * Control buttons that HSG_Chat_appear in the hsg_window.
     */
    private Button hsg_listenButton, hsg_connectButton, hsg_closeButton, 
                   hsg_clearButton, hsg_quitButton, hsg_saveButton, hsg_hsg_sendButton;

    /**
     * Input boxes for connection information (port numbers and host names).
     */
    private TextField hsg_listeningPortInput, hsg_remotePortInput, hsg_remoteHostInput;

    /**
     * Input box for messages that will be sent to the other side of the
     * network connection.
     */
    private TextField hsg_messageInput;

    /**
     * Contains a hsg_transcript of messages sent and received, along with
     * information about the progress and state of the connection.
     */
    private TextArea hsg_transcript;
    
    /**
     * The program's hsg_window.
     */

    private Stage hsg_window;
    
    
    /**
     * Set up the GUI and event handling.
     */
    public void start(Stage stage) {
        hsg_window = stage;
        
        hsg_listenButton = new Button("Listen on port:");
        hsg_listenButton.setOnAction( this::hsg_doAction );
        hsg_connectButton = new Button("Connect to:");
        hsg_connectButton.setOnAction( this::hsg_doAction );
        hsg_closeButton = new Button("Disconnect");
        hsg_closeButton.setOnAction( this::hsg_doAction );
        hsg_closeButton.setDisable(true);
        hsg_clearButton = new Button("Clear hsg_Transcript");
        hsg_clearButton.setOnAction( this::hsg_doAction );
        hsg_hsg_sendButton = new Button("hsg_Send");
        hsg_hsg_sendButton.setOnAction( this::hsg_doAction );
        hsg_hsg_sendButton.setDisable(true);
        hsg_hsg_sendButton.setDefaultButton(true);
        hsg_saveButton = new Button("Save hsg_Transcript");
        hsg_saveButton.setOnAction( this::hsg_doAction );
        hsg_quitButton = new Button("Quit");
        hsg_quitButton.setOnAction( this::hsg_doAction );
        hsg_messageInput = new TextField();
        hsg_messageInput.setOnAction( this::hsg_doAction );
        hsg_messageInput.setEditable(false);
        hsg_transcript = new TextArea();
        hsg_transcript.setPrefRowCount(20);
        hsg_transcript.setPrefColumnCount(60);
        hsg_transcript.setWrapText(true);
        hsg_transcript.setEditable(false);
        hsg_listeningPortInput = new TextField(hsg_defaultPort);
        hsg_listeningPortInput.setPrefColumnCount(5);
        hsg_remotePortInput = new TextField(hsg_defaultPort);
        hsg_remotePortInput.setPrefColumnCount(5);
        hsg_remoteHostInput = new TextField(hsg_defaultHost);
        hsg_remoteHostInput.setPrefColumnCount(18);
        
        HBox hsg_buttonBar = new HBox(5, hsg_quitButton, hsg_saveButton, hsg_clearButton, hsg_closeButton);
        hsg_buttonBar.setAlignment(Pos.CENTER);
        HBox hsg_connectBar = new HBox(5, hsg_listenButton, hsg_listeningPortInput, hsg_connectButton, 
                                      hsg_remoteHostInput, new Label("port:"), hsg_remotePortInput);
        hsg_connectBar.setAlignment(Pos.CENTER);
        VBox topPane = new VBox(8, hsg_connectBar, hsg_buttonBar);
        BorderPane hsg_inputBar = new BorderPane(hsg_messageInput);
        hsg_inputBar.setLeft( new Label("Your Message:"));
        hsg_inputBar.setRight(hsg_hsg_sendButton);
        BorderPane.setMargin(hsg_messageInput, new Insets(0,5,0,5));
        
        BorderPane hsg_root = new BorderPane(hsg_transcript);
        hsg_root.setTop(topPane);
        hsg_root.setBottom(hsg_inputBar);
        hsg_root.setStyle("-fx-border-color: #444; -fx-border-width: 3px");
        hsg_inputBar.setStyle("-fx-padding:5px; -fx-border-color: #444; -fx-border-width: 3px 0 0 0");
        topPane.setStyle("-fx-padding:5px; -fx-border-color: #444; -fx-border-width: 0 0 3px 0");

        Scene scene = new Scene(hsg_root);
        stage.setScene(scene);
        stage.setTitle("Two-user Networked Chat");
        stage.setOnHidden( e -> {
               // If a connection exists when the hsg_window is closed, close the connection.
            if (connection != null) 
                connection.close(); 
        });
        stage.show();

    } // end start()
    
    
    /**
     * A little wrHSG_Chat_apper for showing an error alert.
     */
    private void hsg_errorMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.showAndWait();
    }

    /**
     * Defines responses to buttons.  (In this program, I use one
     * method to handle all the buttons; the source of the event
     * can be used to determine which button was clicked.)
     */
    private void hsg_doAction(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == hsg_listenButton) {
            if (connection == null || 
                    connection.getConnectionState() == ConnectionState.CLOSED) {
                String portString = hsg_listeningPortInput.getText();
                int port;
                try {
                    port = Integer.parseInt(portString);
                    if (port < 0 || port > 65535)
                        throw new NumberFormatException();
                }
                catch (NumberFormatException e) {
                    hsg_errorMessage(portString + "is not a legal port number.");
                    return;
                }
                hsg_connectButton.setDisable(true);
                hsg_listenButton.setDisable(true);
                hsg_closeButton.setDisable(false);
                connection = new ConnectionHandler(port);
            }
        }
        else if (source == hsg_connectButton) {
            if (connection == null || 
                    connection.getConnectionState() == ConnectionState.CLOSED) {
                String portString = hsg_remotePortInput.getText();
                int port;
                try {
                    port = Integer.parseInt(portString);
                    if (port < 0 || port > 65535)
                        throw new NumberFormatException();
                }
                catch (NumberFormatException e) {
                    hsg_errorMessage(portString +"is not a legal port number.");
                    return;
                }
                hsg_connectButton.setDisable(true);
                hsg_listenButton.setDisable(true);
                connection = new ConnectionHandler(hsg_remoteHostInput.getText(),port);
            }
        }
        else if (source == hsg_closeButton) {
            if (connection != null)
                connection.close();
        }
        else if (source == hsg_clearButton) {
            hsg_transcript.setText("");
        }
        else if (source == hsg_quitButton) {
            try {
                hsg_window.hide();
            }
            catch (SecurityException e) {
            }
        }
        else if (source == hsg_saveButton) {
            hsg_doSave();
        }
        else if (source == hsg_hsg_sendButton || source == hsg_messageInput) {
            if (connection != null && 
                    connection.getConnectionState() == ConnectionState.CONNECTED) {
                connection.hsg_send(hsg_messageInput.getText());
                hsg_messageInput.selectAll();
                hsg_messageInput.requestFocus();
            }
        }
    }
    

    /**
     * Save the contents of the hsg_transcript area to a file selected by the user.
     */
    private void hsg_doSave() {
        FileChooser fileDialog = new FileChooser(); 
        fileDialog.setInitialFileName("hsg_transcript.txt");
        fileDialog.setInitialDirectory(new File(System.getProperty("user.home")));
        fileDialog.setTitle("Select File to be Saved");
        File selectedFile = fileDialog.showSaveDialog(hsg_window);
        if (selectedFile == null)
            return;  // User canceled or clicked the dialog's close box.
        PrintWriter out; 
        try {
            FileWriter stream = new FileWriter(selectedFile); 
            out = new PrintWriter( stream );
        }
        catch (Exception e) {
            hsg_errorMessage("Sorry, but an error occurred while\ntrying to open the file:\n" + e);
            return;
        }
        try {
            out.print(hsg_transcript.getText());  // Write text from the TextArea to the file.
            out.close();
            if (out.checkError())   // (need to check for errors in PrintWriter)
                throw new IOException("Error check failed.");
        }
        catch (Exception e) {
            hsg_errorMessage("Sorry, but an error occurred while\ntrying to write the text:\n" + e);
        }    
    }


    /**
     * Add a line of text to the hsg_transcript area.
     * @param message text to be added; a line feed is added at the end
     */
    private void hsg_postMessage(String message) {
        Platform.runLater( () -> hsg_transcript.appendText(message + '\n') );
    }


    /**
     * Defines the thread that handles the connection.  The thread is responsible
     * for opening the connection and for receiving messages.  This class contains
     * several methods that are called by the main class, and that are therefore
     * executed in a different thread.  Note that by using a thread to open the
     * connection, any blocking of the graphical user interface is avoided.  By
     * using a thread for reading messages sent from the other side, the messages
     * can be received and posted to the hsg_transcript asynchronously at the same
     * time as the user is typing and hsg_sending messages.  All changes to the GUI
     * that are made by this class are done using Platform.runLater().
     */
    private class ConnectionHandler extends Thread {

        private volatile ConnectionState state;
        private String remoteHost;
        private int port;
        private ServerSocket listener;
        private Socket socket;
        private PrintWriter out;
        private BufferedReader in;

        /**
         * Listen for a connection on a specified port.  The constructor
         * does not perform any network operations; it just sets some
         * instance variables and starts the thread.  Note that the
         * thread will only listen for one connection, and then will
         * close its server socket.
         */
        ConnectionHandler(int port) {
            state = ConnectionState.LISTENING;
            this.port = port;
            hsg_postMessage("\nLISTENING ON PORT " + port + "\n");
            try { setDaemon(true); }
            catch (Exception e) {}
            start();
        }

        /**
         * Open a connection to specified computer and port.  The constructor
         * does not perform any network operations; it just sets some
         * instance variables and starts the thread.
         */
        ConnectionHandler(String remoteHost, int port) {
            state = ConnectionState.CONNECTING;
            this.remoteHost = remoteHost;
            this.port = port;
            hsg_postMessage("\nCONNECTING TO " + remoteHost + " ON PORT " + port + "\n");
            try { setDaemon(true); }
            catch (Exception e) {}
            start();
        }

        /**
         * Returns the current state of the connection.  
         */
        synchronized ConnectionState getConnectionState() {
            return state;
        }

        /**
         * hsg_Send a message to the other side of the connection, and post the
         * message to the hsg_transcript.  This should only be called when the
         * connection state is ConnectionState.CONNECTED; if it is called at
         * other times, it is ignored.  (Although it is unlikely, it is
         * possible for this method to block, if the system's buffer for
         * outgoing data fills.)
         */
        synchronized void hsg_send(String message) {
            if (state == ConnectionState.CONNECTED) {
                hsg_postMessage("hsg_SEND:  " + message);
                out.println(message);
                out.flush();
                if (out.checkError()) {
                    hsg_postMessage("\nERROR OCCURRED WHILE TRYING TO hsg_SEND DATA.");
                    close();
                }
            }
        }

        /**
         * Close the connection. If the server socket is non-null, the
         * server socket is closed, which will cause its accept() method to
         * fail with an error.  If the socket is non-null, then the socket
         * is closed, which will cause its input method to fail with an
         * error.  (However, these errors will not be reported to the user.)
         */
        synchronized void close() {
            state = ConnectionState.CLOSED;
            try {
                if (socket != null)
                    socket.close();
                else if (listener != null)
                    listener.close();
            }
            catch (IOException e) {
            }
        }

        /**
         * This is called by the run() method when a message is received from
         * the other side of the connection.  The message is posted to the
         * hsg_transcript, but only if the connection state is CONNECTED.  (This
         * is because a message might be received after the user has clicked
         * the "Disconnect" button; that message should not be seen by the
         * user.)
         */
        synchronized private void received(String message) {
            if (state == ConnectionState.CONNECTED)
                hsg_postMessage("RECEIVE:  " + message);
        }

        /**
         * This is called by the run() method when the connection has been
         * successfully opened.  It enables the correct buttons, writes a
         * message to the hsg_transcript, and sets the connected state to CONNECTED.
         */
        synchronized private void connectionOpened() throws IOException {
            listener = null;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
            state = ConnectionState.CONNECTED;
            Platform.runLater( () -> { 
                hsg_closeButton.setDisable(false);
                hsg_hsg_sendButton.setDisable(false);
                hsg_messageInput.setEditable(true);
                hsg_messageInput.setText("");
                hsg_messageInput.requestFocus();
                hsg_postMessage("CONNECTION ESTABLISHED\n");
            });
        }

        /**
         * This is called by the run() method when the connection is closed
         * from the other side.  (This is detected when an end-of-stream is
         * encountered on the input stream.)  It posts a message to the
         * hsg_transcript and sets the connection state to CLOSED.
         */
        synchronized private void connectionClosedFromOtherSide() {
            if (state == ConnectionState.CONNECTED) {
                hsg_postMessage("\nCONNECTION CLOSED FROM OTHER SIDE\n");
                state = ConnectionState.CLOSED;
            }
        }

        /**
         * Called from the finally clause of the run() method to clean up
         * after the network connection closes for any reason.
         */
        private void cleanUp() {
            state = ConnectionState.CLOSED;
            Platform.runLater( () -> {
                hsg_listenButton.setDisable(false);
                hsg_connectButton.setDisable(false);
                hsg_closeButton.setDisable(true);
                hsg_hsg_sendButton.setDisable(true);
                hsg_messageInput.setEditable(false);
                hsg_postMessage("\n*** CONNECTION CLOSED ***\n");
            });
            if (socket != null && !socket.isClosed()) {
                // Make sure that the socket, if any, is closed.
                try {
                    socket.close();
                }
                catch (IOException e) {
                }
            }
            socket = null;
            in = null;
            out = null;
            listener = null;
        }


        /**
         * The run() method that is executed by the thread.  It opens a
         * connection as a client or as a server (depending on which 
         * constructor was used).
         */
        public void run() {
            try {
                if (state == ConnectionState.LISTENING) {
                        // Open a connection as a server.
                    listener = new ServerSocket(port);
                    socket = listener.accept();
                    listener.close();
                }
                else if (state == ConnectionState.CONNECTING) {
                        // Open a connection as a client.
                    socket = new Socket(remoteHost,port);
                }
                connectionOpened();  // Set up to use the connection.
                while (state == ConnectionState.CONNECTED) {
                        // Read one line of text from the other side of
                        // the connection, and report it to the user.
                    String input = in.readLine();
                    if (input == null)
                        connectionClosedFromOtherSide();
                    else
                        received(input);  // Report message to user.
                }
            }
            catch (Exception e) {
                    // An error occurred.  Report it to the user, but not
                    // if the connection has been closed (since the error
                    // might be the expected error that is generated when
                    // a socket is closed).
                if (state != ConnectionState.CLOSED)
                    hsg_postMessage("\n\n ERROR:  " + e);
            }
            finally {  // Clean up before terminating the thread.
                cleanUp();
            }
        }

    } // end nested class ConnectionHandler

}


