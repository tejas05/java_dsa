/*Problem Description
Given an array A and an integer B, find the number of occurrences of B in A.*/

class CountOccurrences {
    public static void main(String[] args) {
        int[] A = {1,2,1};
        int B = 3; 
        int count = 0; 
        for (int i = 0; i < A.length; i++) {
            if (A[i] == B) {
                count++;
            }
        }

        System.out.println("Number of occurrences of " + B + " in A: " + count);
    }
}

