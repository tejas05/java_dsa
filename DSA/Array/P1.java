/*Problem Description
- Given an array A of size N.
- You need to find the sum of the Maximum and Minimum

elements in the given array.*/

class MaxMinSum {
    public static void main(String[] args) {
        int[] A = {1,3,4,1};
        int result = Sum(A);
        System.out.println("Output: " + result);
    }
  static  int Sum(int[] arr) {
        int n = arr.length;
        if (n == 0) {
            return 0;
        }

        int maxEle = Integer.MIN_VALUE;
        int minEle = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (arr[i] > maxEle) {
                maxEle = arr[i];
            }
            if (arr[i] < minEle) {
                minEle = arr[i];
            }
        }
        int sum = maxEle + minEle;
        return sum;
    }
}

