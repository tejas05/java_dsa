import java.util.ArrayList;
import java.util.List;

class RangeSumQueries {
    public static void main(String[] args) {
        int[] A = {1, 2, 3, 4, 5};
        int[][] B = {{0, 3}, {1, 2}};

        int[] result = findRangeSums(A, B);

        System.out.println("Output 1: " + java.util.Arrays.toString(result));
    }

    public static int[] findRangeSums(int[] A, int[][] queries) {
        int n = A.length;
        int m = queries.length;
        int[] results = new int[m];

        for (int i = 0; i < m; i++) {
            int left = queries[i][0];
            int right = queries[i][1];
            int sum = 0;

            for (int j = left; j <= right; j++) {
                sum += A[j];
            }

            results[i] = sum;
        }

        return results;
    }
}

