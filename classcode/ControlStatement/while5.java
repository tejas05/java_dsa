//Take an integers N as input.
//Print perfect square till N.
//Perfect Square: An integers whose square root is a integer
//25->5    :yess
//81->9    :yess
//1->1     :yess
//10->3.13 :no




class While{
	public static void main(String[]tejas){
		int N=30;
		int i=1;
		while(i*i<=N){
			System.out.println(i*i);
			i++;
		}
	}

}
