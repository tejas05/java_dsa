import java.io.*;
import java.util.*;

class StringTokenizerDemo{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		String info=br.readLine();
		System.out.println(info);
		StringTokenizer obj=new StringTokenizer(info,",");
		String token1=obj.nextToken();
		String token2=obj.nextToken();
		int token3=Integer.parseInt(obj.nextToken());
		float token4=Float.parseFloat(obj.nextToken());

		System.out.println("Factory Name=" + token1);
		System.out.println("Section=" + token2);
		System.out.println("flat no=" + token3);
		System.out.println("valuation=" + token4);
	}
}
