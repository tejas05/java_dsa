import java.io.*;
class Demo{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter no of rows:");
                int row=Integer.parseInt(br.readLine());
                for(int i=1;i<=row;i++){
			char ch = 'a';
			int x = 3;
			if(i % 2==0){
				for(int j=1;j<=row;j++){
					if(j%2==0){
						System.out.print(x + " ");
					}
					else{
						System.out.println(ch + " ");
                                	}
					x--;
					ch++;
				}
			}
			else{
				for(int j=1;j<=row;j++){
					if(j%2!=0){
						System.out.print(x + " ");
					}
					else{
						System.out.println( ch + " ");
					}
					x--;
					ch++;
				}
			}
			System.out.println();
                }
        }
}
