import java.io.*;
class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no of rows:");
		int row=Integer.parseInt(br.readLine());
		int N=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print(((N*N)-1)+" ");
				N++;
			}
			System.out.println();
		}
	}
}
