import java.io.*;
class Demo{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter no of rows:");
                int row=Integer.parseInt(br.readLine());
                int x=0;
		int y=1;
                for(int i=1;i<=row;i++){
                        for(int j=1;j<=row;j++){
                                System.out.print(x + " ");
				int z = x + y;
				x=y;
				y=z;
			}
			System.out.println();
		}
		System.out.println();
               
        }
}
