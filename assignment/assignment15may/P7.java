import java.io.*;
class Demo{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter year:");
                int year=Integer.parseInt(br.readLine());
		if(((year % 4==0) && (year %100!=0))||(year %400 ==0)){
			System.out.println("It is a leap year");
		}
		else{
			System.out.println("It is not leap Year");
		}
	}
}

