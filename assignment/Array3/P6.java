//WAP tp palindrome element in an array.



import java.io.*;
class ArrayDemo{
        public static void main( String []tejas)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("Enter Element:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                for(int i=0;i<arr.length;i++){
                        int rev=0;
                        int temp=arr[i];
                        while(arr[i]!=0){
                                int rem=arr[i]%10;
                                rev=rev*10+rem;
                                arr[i]=arr[i]/10;
                                }
                                if(temp==rev)
                                {
                                        System.out.println("Palindrome no "+ temp + " found at index:"+i);
                                }

                }
        }
}
