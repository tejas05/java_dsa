//WAP to reverse each element in an array.


import java.io.*;
class ArrayDemo{
        public static void main( String []tejas)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("Enter Element:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                for(int i=0;i<arr.length;i++){
                        int reverse=0;
                        int rem;
                        for(int j=1;j<arr[i];j++){
                                if(arr[i]!=0){
                                        rem=arr[i]%10;
                                        reverse=reverse*10+rem;
                                        arr[i]=arr[i]/10;
                                }
			}
                                System.out.println("Reverse no:"+ arr[i]);
                        
                }
        }
}
