import java.io.*;
class ArrayDemo{
        public static void main( String []tejas)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("Enter Element:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("count of Digits:");
                for(int i=0;i<arr.length;i++){
                        int count=0;
                        for(int j=1;j<arr[i];j++){
                                while(arr[i]!=0){
					count++;
					arr[i]=arr[i]/10;
				}
			}
			System.out.println(count);
		}
	}
}
