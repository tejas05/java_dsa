import java.io.*;
class ArrayDemo{
        public static void main(String[]tejas)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Array size:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("Enter Element:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int mult=1;
                for(int i=0;i<arr.length;i++){
                        if(arr[i]%2==0){
                                mult=mult*arr[i];
                        }
                }
                System.out.println("Product of even number=" + mult);


        }
}

